package com.citiustech.poc.microservice.patient.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.citiustech.poc.microservice.patient.domain.Patient;
import com.citiustech.poc.microservice.patient.repository.PatientRepository;

@Service
public class PatientServiceImpl implements PatientService {

    private PatientRepository patientRepository;

    @Autowired
    public PatientServiceImpl(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }
    
    @Resource
    public RestTemplate restTemplate;
    
    @Autowired
    private CounterService counterService;
    
	@Override
	public List<Patient> listAll() {
		List<Patient> patients = new ArrayList<>();
        patientRepository.findAll().forEach(patients::add); //fun with Java 8
		return patients;
	}

	@Override
	public Patient getById(long id) {
        return patientRepository.findOne(id);
	}

	@Override
	public Patient saveOrUpdate(Patient patient) throws Exception {
		if(patient.getPatientId() == 0){
			patient.setPatientId(counterService.getNextSequence("patientIdSequence"));
		}
		try{
			patientRepository.save(patient);
		} catch(Exception e) {
			throw new Exception(e);
		}
		return patient;
	}

	@Override
	public void delete(long id) throws Exception {
		try{
			// Delete Patient Details first
//			restTemplate.getForEntity("http://patientdetails/delete/{patientId}", Integer.class, id);
			restTemplate.getForEntity("http://localhost:8092/patientdetails/delete/{patientId}", Integer.class, id);
			// Delete the Patient Record
			patientRepository.delete(id);
		} catch(Exception e) {
			throw new Exception(e.getMessage());
		}
	}

}
