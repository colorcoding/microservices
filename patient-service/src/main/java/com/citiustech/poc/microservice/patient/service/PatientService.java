package com.citiustech.poc.microservice.patient.service;

import java.util.List;

import com.citiustech.poc.microservice.patient.domain.Patient;

public interface PatientService {

	List<Patient> listAll();

    Patient getById(long id);

    Patient saveOrUpdate(Patient patient) throws Exception;

    void delete(long id) throws Exception;
}
