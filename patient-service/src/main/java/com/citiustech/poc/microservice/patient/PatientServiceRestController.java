package com.citiustech.poc.microservice.patient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citiustech.poc.microservice.patient.domain.Patient;
import com.citiustech.poc.microservice.patient.service.PatientService;

@RestController
public class PatientServiceRestController {
	
	private PatientService patientService;
    
    @Autowired
    public void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    @RequestMapping("/")
    public String redirToList(){
        return "redirect:/patient/list";
    }
    
    @RequestMapping(value = ("/patient/list"),method = RequestMethod.GET)
    public ResponseEntity<List<Patient>> getPatients(){
        List<Patient> pList = patientService.listAll();
        return new ResponseEntity<List<Patient>>(pList, HttpStatus.OK);
    }
    
    @RequestMapping(value = ("/patient/show/{id}"), method = RequestMethod.GET)
    public ResponseEntity<Patient> getPatientById(@PathVariable long id){
        Patient patient = patientService.getById(id);
        return new ResponseEntity<Patient>(patient, HttpStatus.OK);
    }
    
    @RequestMapping(value = ("/patient"), method = RequestMethod.POST)
    public ResponseEntity<Void> addOrUpdatePatient(@RequestBody Patient patient){
        patientService.saveOrUpdate(patient);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    @RequestMapping(value = ("/patient/delete/{id}"),method = RequestMethod.GET)
    public ResponseEntity<Void> deletePatient(@PathVariable long id){
        patientService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
