package com.citiustech.poc.microservice.patient.service;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.citiustech.poc.microservice.patient.component.PatientIdSequence;

@Service
public class CounterService {
	
	@Autowired 
	private MongoOperations mongo;
	
	public long getNextSequence(String seqName) {
		 
	    PatientIdSequence counter = mongo.findAndModify(
	      query(where("_id").is(seqName)), 
	      new Update().inc("seq", 1),
	      options().returnNew(true).upsert(true),
	      PatientIdSequence.class);
	       
	    return counter.getSeq();
	  }

}
