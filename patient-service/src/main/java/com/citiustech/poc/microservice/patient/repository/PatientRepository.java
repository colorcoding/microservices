package com.citiustech.poc.microservice.patient.repository;

import org.springframework.data.repository.CrudRepository;

import com.citiustech.poc.microservice.patient.domain.Patient;

public interface PatientRepository extends CrudRepository<Patient, Long>{

}
