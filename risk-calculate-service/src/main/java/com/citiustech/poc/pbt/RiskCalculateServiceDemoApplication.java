package com.citiustech.poc.pbt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiskCalculateServiceDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiskCalculateServiceDemoApplication.class, args);
	}
}
