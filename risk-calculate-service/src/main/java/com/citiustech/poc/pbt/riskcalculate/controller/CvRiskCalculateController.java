package com.citiustech.poc.pbt.riskcalculate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.citiustech.poc.pbt.riskcalculate.domain.CvRiskInput;
import com.citiustech.poc.pbt.riskcalculate.domain.CvRiskOutput;
import com.citiustech.poc.pbt.riskcalculate.service.CvRiskCalculateService;
import org.springframework.http.HttpStatus;

@Controller
public class CvRiskCalculateController {
	
	private CvRiskCalculateService riskService;
	
	@Autowired
	public void setRiskService(CvRiskCalculateService riskService) {
		this.riskService = riskService;
	}

	@RequestMapping(value = "/calculaterisk", method = RequestMethod.POST)
    public ResponseEntity<CvRiskOutput> calculateRisk(@RequestBody CvRiskInput input){
		CvRiskOutput res = riskService.calculateRisk(input);
        return new ResponseEntity<CvRiskOutput>(res, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/calculaterelativerisk", method = RequestMethod.POST)
    public ResponseEntity<CvRiskOutput> calculateRelativeRisk(@RequestBody CvRiskInput input){
		CvRiskOutput res = riskService.calculateRelativeRisk(input);
        return new ResponseEntity<CvRiskOutput>(res, HttpStatus.OK);
    }

}
