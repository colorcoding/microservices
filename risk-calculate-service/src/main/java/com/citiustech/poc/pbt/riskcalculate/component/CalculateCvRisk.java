package com.citiustech.poc.pbt.riskcalculate.component;

import org.springframework.stereotype.Component;

import com.citiustech.poc.pbt.riskcalculate.domain.CvRiskInput;
import com.citiustech.poc.pbt.riskcalculate.domain.CvRiskOutput;

@Component
public class CalculateCvRisk {
	
//	public static void main(String[] args) {
//		CvRiskInput inp = new CvRiskInput(CvRiskInput.Gender.Male, 52, 155, 3.2f, 1.2f, true, false, false);
//		calculateRisk(inp);
//	}
	
	/**
	 * Calculates CardioVascular Risk using Framingham Formula
	 * 
	 * @param input : CvRiskInput
	 * @return result : CvRiskOutput
	 */
	public static CvRiskOutput calculateRisk(CvRiskInput input) {
		CvRiskOutput res = new CvRiskOutput();
		String gender = input.getGender().name();
		// Calculate
		double riskFactor = (gender.equals("Female") ?
				calculateRiskFactorWomen(input) : calculateRiskFactorMen(input));
		double risk = calculateRisk (riskFactor, gender);
		System.out.println("Risk Factor: "+ riskFactor + ", Risk: " + risk);
		res.setRiskFactor(riskFactor);
		res.setRisk(risk);
		return res;
	}

	private static double calculateRisk(double riskFactor, String gender) {
		return (100 * 
				(1- 
				(Math.pow((gender.equals("Female") ? RISK_PERIOD_FACTOR_WOMEN : RISK_PERIOD_FACTOR_MEN), 
				Math.exp(riskFactor)
				))));
	}

	private static double calculateRiskFactorWomen(CvRiskInput input) {
		return ((Math.log(input.getAge()) * AGE_FACTOR_WOMEN) +
				(Math.log(input.getLdl()) * TOTAL_CHOL_FACTOR_WOMEN) +
				(Math.log(input.getHdl()) * HDL_CHOL_FACTOR_WOMEN) +
				(Math.log(input.getSbp()) * (input.isHyptensionPresent()
						? SBP_FACTOR_WOMEN_YES : SBP_FACTOR_WOMEN_NO)) +
				(input.isSmoker() ? SMOKING_FACTOR_WOMEN :  0) + 
				(input.isDiabetesPresent() ? DIABETES_WOMEN :  0) - 
				AVG_RISK_WOMEN
				);
	}
	
	private static double calculateRiskFactorMen(CvRiskInput input) {
		return ((Math.log(input.getAge()) * AGE_FACTOR_MEN) +
				(Math.log(input.getLdl()) * TOTAL_CHOL_FACTOR_MEN) +
				(Math.log(input.getHdl()) * HDL_CHOL_FACTOR_MEN) +
				(Math.log(input.getSbp()) * (input.isHyptensionPresent()
						? SBP_FACTOR_MEN_YES : SBP_FACTOR_MEN_NO)) +
				(input.isSmoker() ? SMOKING_FACTOR_MEN :  0) + 
				(input.isDiabetesPresent() ? DIABETES_MEN :  0) - 
				AVG_RISK_MEN
				);
	}
	
	private static final double AGE_FACTOR_WOMEN = 2.32888;
	private static final double AGE_FACTOR_MEN = 3.06117;
	private static final double TOTAL_CHOL_FACTOR_WOMEN = 1.20904;
	private static final double TOTAL_CHOL_FACTOR_MEN =  1.12370;
	private static final double HDL_CHOL_FACTOR_WOMEN = -0.70833;
	private static final double HDL_CHOL_FACTOR_MEN = -0.93263;
	private static final double SBP_FACTOR_WOMEN_NO = 2.76157;
	private static final double SBP_FACTOR_WOMEN_YES = 2.82263;
	private static final double SBP_FACTOR_MEN_NO = 1.93303;
	private static final double SBP_FACTOR_MEN_YES = 1.99881;
	private static final double SMOKING_FACTOR_WOMEN = 0.52873;
	private static final double SMOKING_FACTOR_MEN = 0.65451;
	private static final double DIABETES_WOMEN = 0.69154;
	private static final double DIABETES_MEN = 0.57367;
	private static final double AVG_RISK_WOMEN = 26.1931;
	private static final double AVG_RISK_MEN = 23.9802;
	private static final double RISK_PERIOD_FACTOR_WOMEN = 0.95012;
	private static final double RISK_PERIOD_FACTOR_MEN = 0.88936;
}
