package com.citiustech.poc.pbt.riskcalculate.domain;

public class CvRiskInput {
	
	public enum Gender {Male, Female};
	
	private String name;
	private Address address;
	private Gender gender;
	private int age;
	private int sbp;
	private float ldl;
	private float hdl;
	private boolean hyptensionPresent;
	private boolean smoker;
	private boolean diabetesPresent;
	
	public CvRiskInput(){ super();	}
	
	public CvRiskInput(String name, Address address, Gender gender, int age, int sbp, float ldl,
			float hdl, boolean hyptensionPresent, boolean smoker, boolean diabetesPresent) {
		super();
		this.name = name;
		this.address = address;
		this.gender = gender;
		this.age = age;
		this.sbp = sbp;
		this.ldl = ldl;
		this.hdl = hdl;
		this.hyptensionPresent = hyptensionPresent;
		this.smoker = smoker;
		this.diabetesPresent = diabetesPresent;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSbp() {
		return sbp;
	}
	public void setSbp(int sbp) {
		this.sbp = sbp;
	}
	public float getLdl() {
		return ldl;
	}
	public void setLdl(float ldl) {
		this.ldl = ldl;
	}
	public float getHdl() {
		return hdl;
	}
	public void setHdl(float hdl) {
		this.hdl = hdl;
	}
	public boolean isHyptensionPresent() {
		return hyptensionPresent;
	}
	public void setHyptensionPresent(boolean hyptensionPresent) {
		this.hyptensionPresent = hyptensionPresent;
	}
	public boolean isSmoker() {
		return smoker;
	}
	public void setSmoker(boolean smoker) {
		this.smoker = smoker;
	}
	public boolean isDiabetesPresent() {
		return diabetesPresent;
	}
	public void setDiabetesPresent(boolean diabetesPresent) {
		this.diabetesPresent = diabetesPresent;
	}
}
