package com.citiustech.poc.pbt.riskcalculate.domain;

public class CvRiskOutput {
	
	private double riskfactor;
	private double risk;
	private double relativerisk;
	
	public CvRiskOutput() {
		super();
	}

	public CvRiskOutput(double riskFactor, double risk, double relativeRisk) {
		super();
		this.riskfactor = riskFactor;
		this.risk = risk;
		this.relativerisk = relativeRisk;
	}
	
	public double getRiskFactor() {
		return riskfactor;
	}
	public void setRiskFactor(double riskFactor) {
		this.riskfactor = riskFactor;
	}
	public double getRisk() {
		return risk;
	}
	public void setRisk(double risk) {
		this.risk = risk;
	}
	public double getRelativeRisk() {
		return relativerisk;
	}
	public void setRelativeRisk(double relativeRisk) {
		this.relativerisk = relativeRisk;
	}

	
}
