package com.citiustech.poc.pbt.riskcalculate.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citiustech.poc.pbt.riskcalculate.component.CalculateAvgRiskWithStates;
import com.citiustech.poc.pbt.riskcalculate.component.CalculateCvRisk;
import com.citiustech.poc.pbt.riskcalculate.domain.CvRiskInput;
import com.citiustech.poc.pbt.riskcalculate.domain.CvRiskOutput;

@Service
public class CvRiskCalculateService {
	
	private CalculateCvRisk riskComponent;
	
	@Autowired
	public CvRiskCalculateService(CalculateCvRisk riskComp) {
		this.riskComponent = riskComp;
	}
	
	public CvRiskOutput calculateRisk (CvRiskInput input) {
		CvRiskOutput res = new CvRiskOutput();
		// Formula
		res = riskComponent.calculateRisk(input);
		return res;
	}
	
	/**
	 * Calculate the Relative Risk from 10 years cumulative calc.
	 * @param input
	 * @return
	 */
	public CvRiskOutput calculateRelativeRisk (CvRiskInput input) {
		CvRiskOutput res = calculateRisk(input); // Calc present yr risk
		double avgRisk = getAverageRisk(input, res);
		res.setRelativeRisk(Precision.round((CalculateAvgRiskWithStates.
				calculateRelativeRisk(input, avgRisk)), 3));
//		res.setRelativeRisk(CalculateAvgRiskWithStates.
//				calculateRelativeRisk(input, avgRisk));
		System.out.println("Risk : " + res.getRisk() + "RiskFactor : " + res.getRiskFactor()
		 + " RelativeRisk : " + res.getRelativeRisk());
		return res;
	}

	private double getAverageRisk(CvRiskInput input, CvRiskOutput res) {
		double avgRisk = 0;
		if(input.getAge() >= 30 && input.getAge() <= 40){
			avgRisk = calcAvgRisk(res.getRisk(), 0.2);
		} else if(input.getAge() >= 40 && input.getAge() <= 50) {
			avgRisk = calcAvgRisk(res.getRisk(), 0.25);
		} else if(input.getAge() >= 50 && input.getAge() <= 60) {
			avgRisk = calcAvgRisk(res.getRisk(), 0.5);
		}
		return avgRisk;
	}

	private double calcAvgRisk(double currentRisk, double factor) {
		List<Double> list = new ArrayList<Double>();
		list.add(currentRisk);		
		for(int i=1 ; i<10; i++) {
			list.add(currentRisk + (factor*i));
		}
		double risk = list.stream().mapToDouble(f -> f.doubleValue()).sum();
		return (risk/10);
	}
}
