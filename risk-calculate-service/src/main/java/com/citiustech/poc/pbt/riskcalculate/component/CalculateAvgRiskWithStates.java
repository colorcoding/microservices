package com.citiustech.poc.pbt.riskcalculate.component;

import com.citiustech.poc.pbt.riskcalculate.domain.CvRiskInput;

public class CalculateAvgRiskWithStates {

	public static double calculateRelativeRisk(CvRiskInput input, double avgRisk) {
		double relativeRisk = 0F;
		if(input.getAddress().getState().startsWith("A") || input.getAddress().getState().startsWith("C")) {
			relativeRisk = avgRisk / 20.0; // Dividend is just a factor for a state
		} else if(input.getAddress().getState().startsWith("D") || input.getAddress().getState().startsWith("F")) {
			relativeRisk = avgRisk / 15.0; // Dividend is just a factor for a state
		} else if(input.getAddress().getState().startsWith("G") || input.getAddress().getState().startsWith("H")) {
			relativeRisk = avgRisk / 15.0; // Dividend is just a factor for a state
		} else if(input.getAddress().getState().startsWith("I") || input.getAddress().getState().startsWith("K")) {
			relativeRisk = avgRisk / 15.0; // Dividend is just a factor for a state
		} else if(input.getAddress().getState().startsWith("L") || input.getAddress().getState().startsWith("M")) {
			relativeRisk = avgRisk / 10.0; // Dividend is just a factor for a state
		} else if(input.getAddress().getState().startsWith("N") || input.getAddress().getState().startsWith("O")) {
			relativeRisk = avgRisk / 5.0; // Dividend is just a factor for a state
		} else if(input.getAddress().getState().startsWith("P") || input.getAddress().getState().startsWith("R")) {
			relativeRisk = avgRisk / 20.0; // Dividend is just a factor for a state
		} else if(input.getAddress().getState().startsWith("S") || input.getAddress().getState().startsWith("T")) {
			relativeRisk = avgRisk / 15.0; // Dividend is just a factor for a state
		} else if(input.getAddress().getState().startsWith("U") || input.getAddress().getState().startsWith("V")) {
			relativeRisk = avgRisk / 25.0; // Dividend is just a factor for a state
		} else if(input.getAddress().getState().startsWith("W")) {
			relativeRisk = avgRisk / 25.0; // Dividend is just a factor for a state
		}
		return relativeRisk;
	}

	public enum State {
		AL, 
		AK,
		AZ,
		AR,
		CA,
		CT,
		DE,
		FL,
		GA,
		HI,
		ID,
		IL,
		IN,
		IO,
		KS,
		KY,
		LA,
		ME,
		MD,
		MA,
		MI,
		MN,
		MS,
		MO,
		MT,
		NE,
		NV,
		NH,
		NJ,
		NM,
		NY,
		NC,
		ND,
		OH,
		OK,
		OR,
		PA,
		RI,
		SC,
		SD,
		TN,
		TX,
		UT,
		VT,
		VA,
		WA,
		WV,
		WI,
		WY
	};
}
