<h3>About</h3>
This service is a REST Api created using Spring Boot
It calculates risk and risk factor for a cardiovascular disease based on Framingham's formula. 
However, the relative risk is an approx number and just used for demo purpose. The relative risk is calculated for avg of 10 years for a patient living in a particular US state.


Please use the below JSON format for request

<h5>POST request URL to calculate risk</h5>
http://localhost:8080/calculaterisk


<h5>POST request URL to calculate relative risk</h5>
http://localhost:8080/calculaterelativerisk

<h5>Request JSON for both URLs</h5>
{
  "name":"QWERTYUUI",
  "address":{"street":"abcdeasdf", "city":"qwertydsff", "state":"AK"},
  "gender": "Female",
  "sbp": 155,
  "ldl": 3.2,
  "hdl": 1.2,
  "hypertension": true,
  "smoker": false,
  "diabetes": false,
  "age": 52
}

<h5>Response JSON for Risk</h5>
{
    "riskFactor": -2.456
    "risk": 0.123
    "relativeRisk": 0.0
}

<h5>Response JSON for Relative Risk</h5>
{
    "riskFactor": -2.456
    "risk": 0.123
    "relativeRisk": 1.6
}

