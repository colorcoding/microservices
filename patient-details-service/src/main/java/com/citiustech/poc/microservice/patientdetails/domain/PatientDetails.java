package com.citiustech.poc.microservice.patientdetails.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class PatientDetails {
	@Id
    private long patientId;
	private String allergies ;
    private String majorDiagnosis;
    private String majorMedicalHistory;
    private int bloodPressure;
    private int pulseRate;
    private String affetedOrgan;
    private float height;
    private int weight;
        
    public PatientDetails() {
		super();
	}

	public PatientDetails(long patientId, String allergies, String majorDiagnosis, String majorMedicalHistory,
			int bloodPressure, int pulseRate, String affetedOrgan, float height, int weight) {
		super();
		this.patientId = patientId;
		this.allergies = allergies;
		this.majorDiagnosis = majorDiagnosis;
		this.majorMedicalHistory = majorMedicalHistory;
		this.bloodPressure = bloodPressure;
		this.pulseRate = pulseRate;
		this.affetedOrgan = affetedOrgan;
		this.height = height;
		this.weight = weight;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public String getAllergies() {
		return allergies;
	}

	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}

	public String getMajorDiagnosis() {
		return majorDiagnosis;
	}

	public void setMajorDiagnosis(String majorDiagnosis) {
		this.majorDiagnosis = majorDiagnosis;
	}

	public String getMajorMedicalHistory() {
		return majorMedicalHistory;
	}

	public void setMajorMedicalHistory(String majorMedicalHistory) {
		this.majorMedicalHistory = majorMedicalHistory;
	}

	public int getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(int bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public int getPulseRate() {
		return pulseRate;
	}

	public void setPulseRate(int pulseRate) {
		this.pulseRate = pulseRate;
	}

	public String getAffetedOrgan() {
		return affetedOrgan;
	}

	public void setAffetedOrgan(String affetedOrgan) {
		this.affetedOrgan = affetedOrgan;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	
}
